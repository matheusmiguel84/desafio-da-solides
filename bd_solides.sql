-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 17/02/2020 às 02:26
-- Versão do servidor: 10.4.11-MariaDB
-- Versão do PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bd_solides`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbl_pontos`
--

CREATE TABLE `tbl_pontos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ocorrencia` datetime DEFAULT current_timestamp(),
  `tipo` enum('Inicio','Fim','Inicio_intervalo','Fim_intervalo') NOT NULL,
  `usuario_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbl_pontos`
--

INSERT INTO `tbl_pontos` (`id`, `ocorrencia`, `tipo`, `usuario_id`) VALUES
(19, '2019-01-13 21:00:24', 'Inicio', 1),
(20, '2019-01-13 21:00:25', 'Fim', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbl_usuario`
--

CREATE TABLE `tbl_usuario` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(50) NOT NULL,
  `sobrenome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` char(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id`, `nome`, `sobrenome`, `email`, `senha`) VALUES
(1, 'Matheus', 'Miguel', 'matheusmiguel84@gmail.com', 'mmmmmmmm'),
(2, 'Alexandre', 'Miguel', 'alexandremiguel202@gmail.com', 'aaaaaaaa'),
(3, 'Matheus', 'Lima', 'matheus@gmail.com', '9aee390f19345028f03bb16c588550e1'),
(6, 'Nagela', 'Roberta', 'nagela@gmail.com', 'f7077c3ce8140d704c023ad4c773fef5'),
(7, 'matheus', 'miguel', 'mama@mama.com', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(8, 'Adalberto ', 'Guimarães', 'adalberto@hotmail.com', '2a7d94e6d20ed9be4edca6f5ebe5e0ab'),
(9, 'Aguinaldo', 'cebola', 'cebolinha@teste.com', '2a7d94e6d20ed9be4edca6f5ebe5e0ab'),
(10, 'peixe', 'teste', 'dasa@fe.com', '7c9b6b461e88ca6bbec624affb7634a1'),
(11, 'Guilherme', 'Dias', 'guigui@gmail.com', '2a7d94e6d20ed9be4edca6f5ebe5e0ab'),
(12, 'Matheus', 'Miguel', 'matheusmiguel@gmail.com', '7130ba0db69d27e8433ed945f0a101a5');

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `viewpontosusuarios`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `viewpontosusuarios` (
`nome` varchar(50)
,`ocorrencia` datetime
);

-- --------------------------------------------------------

--
-- Estrutura para view `viewpontosusuarios`
--
DROP TABLE IF EXISTS `viewpontosusuarios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewpontosusuarios`  AS  select `tbl_usuario`.`nome` AS `nome`,`tbl_pontos`.`ocorrencia` AS `ocorrencia` from (`tbl_usuario` join `tbl_pontos` on(`tbl_usuario`.`id` = `tbl_pontos`.`usuario_id`)) ;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `tbl_pontos`
--
ALTER TABLE `tbl_pontos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Índices de tabela `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `tbl_pontos`
--
ALTER TABLE `tbl_pontos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de tabela `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `tbl_pontos`
--
ALTER TABLE `tbl_pontos`
  ADD CONSTRAINT `tbl_pontos_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `tbl_usuario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
