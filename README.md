# Solides Processo Seletivo #

### Tarefa ###

Criar um WebApp* para estagiários apontarem hora de trabalho. Este Webapp deve conter:

- Tela de login
- Tela de cadastro
- Controle de sessão via login
- Tela para apontar hora (chegada na empresa, hora de almoço, saída da empresa)
- Tela de histórico de apontamentos do usuário
- Design responsivo que utilize JQuery e Bootstrap
- Arquivo de DUMP do Mysql, para recriar o banco de dados em outro ambiente
- Um readme.txt explicando como fazer funcionar o seu sistema

### Como instalar ###

1. Instale um servidor/ambiente para rodar o PHP/MySql.

2. Importe o arquivo bd_solides.sql para o MySql.


