<div class="card">
    <div class="header">
        <h4 class="title">Registre-se</h4>
    </div>
    <div class="content">
        <?php echo form_open('Registro/insert_registro'); ?>
        <center><img src="img/solides-logo.png" class="form-logo" width="400"/></center>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Primeiro nome <span class="text-danger">*</span></label>
                    <input required type="text" class="form-control border-input" id="nome"
                           name="nome" placeholder="Primeiro nome" value="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Sobrenome <span class="text-danger">*</span></label>
                    <input required type="text" class="form-control border-input" id="sobrenome"
                           name="sobrenome" placeholder="Sobrenome" value="">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="EmailInput">Email <span class="text-danger">*</span></label>
                    <input required type="email" id="Email" name="Email"
                           class="form-control border-input" placeholder="Email">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Senha <span class="text-danger">*</span></label>
                    <input required type="password" id="Senha" name="Senha"
                           class="form-control border-input" placeholder="Digite a Senha">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                <label>Confirmar senha <span class="text-danger">*</span></label>
                    <input required type="password" id="csenha" name="csenha"
                           class="form-control border-input" placeholder="Confirme a Senha">
                </div>
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-info btn-fill btn-wd">Registar Dados</button>
            <a  href="/gerenciador/login" type="submit" class="btn btn-info btn-fill btn-wd">Login</a>
        </div>
        <div class="clearfix"></div>
        <?php echo form_close(); ?>
    </div>
</div>