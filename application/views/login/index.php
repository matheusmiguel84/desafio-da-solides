<div class="card">
    <div class="header">
        <h4 class="title">Login</h4>
    </div>
    <div class="content">
        <form action="<?php echo base_url();?>Login/process" method='post' name='process' enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <center><img src="img/solides-logo.png" class="form-logo" width="400"/></center>
                    <br>
                    <?php if ($this->session->flashdata('error')) { ?>
                        <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                    <?php } ?>
                    <br>
                    <label>Email<span class="text-danger">*</span></label>
                    <input required type="email" class="form-control border-input" id="Email"
                           name="Email" placeholder="Email" value="">
                </div>
                <div class="form-group">
                    <label>Senha</label>
                    <input required type="password" class="form-control border-input" id="Senha"
                           name="Senha" placeholder="Senha" value="">
                </div>  
                <div class="text-center">
            <button type="submit" class="btn btn-info btn-fill btn-wd">Entrar</button>
            <a href="/gerenciador/registro" type="" class="btn btn-info btn-fill btn-wd">Registre-se</a>
        </div>          
            </div>
        <div class="clearfix"></div>
        <?php echo form_close(); ?>
    </div>
</div>