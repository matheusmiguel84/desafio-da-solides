<?php
class MY_Controller extends CI_Controller
{

    private $data = [];


    public function set_titulo($titulo)
    {
        $this->data['titulo'] = $titulo;
    }

    public function set_pagina($pagina, $model = null)
    {
        $this->data['pagina'] = $this->load->view($pagina, $model, true);
    }

    public function set_layout($layout)
    {
        $this->load->view($layout, $this->data);
    }

    public function verfica_vazio($data, $key)
    {

        if (empty($data[$key])) {
            echo json_encode(['error', 'Preencha o campo ' . $key . ' corretamente.']);
            exit();
        }

    }

    public function sendMail($email = null, $subject = null, $message = null)
    {
        if ($email != null && $subject != null && $message != null) {

            $config['charset'] = 'utf-8';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            $this->email->from('email', 'Nome da Empresa');
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($message);

            $send = $this->email->send();
            return $send;
        }
    }

    public function upload($path, $nameImagem)
    {
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($nameImagem)) {
            return $this->upload->data();
        } else {
            return null;
        }
    }

    public function set_hash($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {

        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        $retorno = '';
        $caracteres = '';
        $caracteres .= $lmin;

        if ($maiusculas) $caracteres .= $lmai;
        if ($numeros) $caracteres .= $num;
        if ($simbolos) $caracteres .= $simb;

        $len = strlen($caracteres);

        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand - 1];
        }

        return $retorno;
    }

}