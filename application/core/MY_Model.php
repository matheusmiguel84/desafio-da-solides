<?php

class MY_Model extends CI_Model
{
    private $tabela;

    public function set_tabela($tabela) {
        $this->tabela = $tabela;
    }

    public function get_tabela() {
        return $this->tabela;
    }

    public function get($param = null) {

        if ($param != null) {
            $this->db->where($param);
        }

        $get = $this->db->get($this->get_tabela());
        return $get;
    }

    public function create($data) {
        $create = $this->db->insert($this->get_tabela(), $data);
        if ($create) {
            return ['success', 'Cadastro realizado com sucesso.'];
        } else {
            return ['error', 'Não foi possível realizar o cadastro'];
        }
    }

    public function update($data, $id) {
        $update = $this->db->update($this->get_tabela(), $data, ['Id' => $id]);
        if ($update) {
            return ['success', 'Cadastro atualizado com sucesso.'];
        } else {
            return ['error', 'Não foi possível atualizar o cadastro'];
        }
    }

    public function remover($id) {
        $deletar = $this->db->delete($this->get_tabela(), ['Id' => $id]);
        if ($deletar) {
            return ['success', 'Cadastro removido com sucesso.'];
        } else {
            return ['error', 'Não foi possível remover o cadastro'];
        }
    }

}