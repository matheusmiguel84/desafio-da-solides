<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Registro extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('RegistrosModel');

    }

    public function index()
    {
        $this->set_titulo('Registro');
        $this->set_pagina('registro/index');
        $this->set_layout('template/gerenciador');
    }

    function insert_registro() {
        $registro = array(
            'nome' => $this->input->post('nome'),
            'sobrenome' => $this->input->post('sobrenome'),
            'email' => $this->input->post('Email'),
            'senha' => md5($this->input->post('Senha'))
        );
        $this->RegistrosModel->novo_registro($registro);
        $this->session->set_flashdata('success', 'Cadastro realizado com sucesso, Favor realizar o login para entrar no sistema.');
        redirect('Login');
    }
}
