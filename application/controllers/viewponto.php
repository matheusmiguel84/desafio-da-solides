<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class viewponto extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('ViewModel');

    }

    public function index()
    {
        $this->set_titulo('Viewponto');
        $this->load->model('ViewModel');
        $this->load->library('session');
        $this->set_pagina('viewponto/index');
        $this->set_layout('template/gerenciador');
    }
    public function dispdata()
	{
	$result['$data']=$this->ViewModel->buscatodos();
	$this->load->view('Viewponto/index',$result);
	}

}
