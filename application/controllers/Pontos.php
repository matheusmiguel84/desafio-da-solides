<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pontos extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('PontosModel');

    }

    public function index()
    {
        $this->set_titulo('Pontos');
        $this->load->library('session');
        $this->set_pagina('Pontos/index');
        $this->set_layout('template/gerenciador');
    }

    public function insert_ponto(){
        $registro = array(
            'ocorrencia' => $this->input->post('data')
        );
        $this->PontosModel->novo_registro($registro);
        $this->session->set_flashdata('success', 'Cadastro realizado com sucesso, Favor realizar o login para entrar no sistema.');
        redirect('Pontos');
    }
}