<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->set_titulo('Login');
        $this->load->library('session');
        $this->set_pagina('login/index');
        $this->set_layout('template/gerenciador');
    }
    public function process(){
        // Load the model
        $this->load->model('LoginModel');
        // Validate the user can login
        $result = $this->LoginModel->validate();
        // Now we verify the result
        if(! $result){
            // If user did not validate, then show them login page again
            $this->session->set_flashdata('error', 'Invalid username and/or password.');
            redirect('Login');
        }else{
            // If user did validate, 
            // Send them to members area
            redirect('Pontos');
        }        
    }
}
