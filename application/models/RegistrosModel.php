<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class RegistrosModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->set_tabela('tbl_usuario');
    }

    public function novo_registro($registro)
    {
        $this->db->insert('tbl_usuario', $registro);
    }
    
}
