<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class LoginModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->set_tabela('tbl_usuario');
    }
    
    public function validate(){
        // grab user input
        $username = $this->security->xss_clean($this->input->post('Email'));
        $password = $this->security->xss_clean($this->input->post('Senha'));

        // Prep the query
        $this->db->where('Email', $username);
        $this->db->where('Senha', $password);

        // Run the query
        $query = $this->db->get('tbl_usuario');
        // Let's check if there are any results
        if($query->num_rows() == 1)
        {
            // If there is a user, then create session data
            $row = $query->row();
            $data = array(
                    'id' => $row->id,
                    'nome' => $row->nome,
                    'sobrenome' => $row->sobrenome,
                    'email' => $row->email,
                    'senha' => $row->senha,
                    'validated' => true
                    );
            $this->session->set_userdata($usuario);
            return true;
        }
        // If the previous process did not validate
        // then return false.
        return false;
    }
}
