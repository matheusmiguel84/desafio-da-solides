<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PontosModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->set_tabela('tbl_pontos');
    }
    
    public function novo_registro($registro)
    {
        $this->db->insert('tbl_pontos', $registro);
    }
}
