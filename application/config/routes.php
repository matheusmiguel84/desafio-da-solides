<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$route['default_controller'] = "Login/index";
$route['404_override'] = '';
$route['login'] = 'Login/index';
$route['registro'] = 'Registro/index';
$route['pontos'] = 'Pontos/index';
$route['viewponto'] = 'Viewponto/index';


$route['success-flash'] = 'MyFlashController/success';
$route['error-flash'] = 'MyFlashController/error';
$route['warning-flash'] = 'MyFlashController/warning';
$route['info-flash'] = 'MyFlashController/info';


/* End of file routes.php */
/* Location: ./application/config/routes.php */