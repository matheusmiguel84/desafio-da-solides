<?php
?>

<footer class="footer">
    <div class="container-fluid">
        <div class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script>, Criado por <a href="">Matheus Miguel</a>
        </div>
    </div>
</footer>
